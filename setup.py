from distutils.core import setup
setup(
  name = 'rp_utils',
  packages = ['rp_utils'], # this must be the same as the name above
  version = '0.1',
  description = 'RP utils',
  author = 'Denis',
  author_email = 'denis@rewardpod.com',
  url = 'https://github.com/peterldowns/mypackage', # use the URL to the github repo
  download_url = 'https://github.com/peterldowns/mypackage/tarball/0.1', # I'll explain this in a second
  keywords = ['rewardpod'], # arbitrary keywords
  classifiers = [],
)